libevent-perl (1.28-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Update lintian override info format in d/libevent-perl.lintian-overrides on line 2.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 16 Dec 2022 16:24:22 +0000

libevent-perl (1.28-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * Update 'DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow' to '=+all'.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

  [ gregor herrmann ]
  * Import upstream version 1.28.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.0.
  * Set Rules-Requires-Root: no.

 -- gregor herrmann <gregoa@debian.org>  Sun, 26 Sep 2021 16:52:53 +0200

libevent-perl (1.27-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Remove Nathan handler from Uploaders. Thanks for your work!
  * Remove Rene Mayorga from Uploaders. Thanks for your work!

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 1.27.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.2.1.
  * Bump debhelper compatibility level to 10.
  * Remove trailing whitespace from debian/*.

 -- gregor herrmann <gregoa@debian.org>  Mon, 26 Nov 2018 20:14:22 +0100

libevent-perl (1.26-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.26
  * Remove patch fixing typo in POD, applied by upstream

 -- Lucas Kanashiro <kanashiro@debian.org>  Sun, 03 Jul 2016 11:50:56 -0300

libevent-perl (1.25-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Lucas Kanashiro ]
  * Import upstream version 1.25
  * Drop patch, already applied by upstream
  * Update years of Debian packaging copyright
  * Declare compliance with Debian policy 3.9.8
  * debian/rules: DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow
  * Create patch fixing typos in manpage

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Mon, 13 Jun 2016 12:54:53 -0300

libevent-perl (1.24-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Lucas Kanashiro ]
  * Imported Upstream version 1.24
  * debian/patches: remove pod-encoding.patch, fixed by upstream
  * debian/control:
    - bump Standars-Version to 3.9.6
    - add test suite with autopkgtest-pkg-perl
  * debian/patch: add fix_bareword_not_allowed.patch (fix
    autopkgtest-pkg-perl tests)
  * debian/upstream/metadata: help upstream git integration
  * debian/copyright: fix typo: pport.h -> ppport.h

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Fri, 24 Jul 2015 12:26:40 -0300

libevent-perl (1.23-1) unstable; urgency=medium

  * New upstream release.
  * Replace fix-example-wrong-path-for-interpreter.patch by an override in
    debian/rules.
  * Fix typo in previous changelog stanza.

 -- gregor herrmann <gregoa@debian.org>  Sat, 12 Jul 2014 14:31:48 +0200

libevent-perl (1.22-1) unstable; urgency=medium

  * New upstream release.
  * Update years of packaging copyright.
  * Drop Recommends on libtime-hires-perl. Package doesn't exist,
    Time::HiRes is in perl core since ages.
  * Add a doc-base registration file.

 -- gregor herrmann <gregoa@debian.org>  Mon, 30 Jun 2014 19:43:06 +0200

libevent-perl (1.21-2) unstable; urgency=low

  * Team upload

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Florian Schlichting ]
  * Switch dh compatibility to level 9 to enable passing of hardening flags
  * Declare compliance with Debian Policy 3.9.5
  * Fix a POD warning (patch)
  * Add a linitian override about lacking hardening flags

 -- Florian Schlichting <fsfs@debian.org>  Sun, 13 Apr 2014 17:47:47 +0200

libevent-perl (1.21-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Alessandro Ghedini ]
  * New upstream release (Closes: #676273)

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Alessandro Ghedini ]
  * Email change: Alessandro Ghedini -> ghedo@debian.org

  [ Nathan Handler ]
  * Email change: Nathan Handler -> nhandler@debian.org

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Dominic Hargreaves ]
  * New upstream release

 -- Dominic Hargreaves <dom@earth.li>  Sun, 05 May 2013 23:53:40 +0100

libevent-perl (1.15-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release 1.13

  [ Alessandro Ghedini ]
  * New upstream release
  * Switch to 3.0 (quilt) format
  * Bump Standards-Version to 3.9.2 (no changes needed)
  * Add myself to Uploaders
  * debian/copyright:
    - Replace "Debian GNU/Linux" with just "Debian"
    - Fix copyright-refers-to-symlink-license
  * Add fix-example-wrong-path-for-interpreter.patch

  [ gregor herrmann ]
  * Install new tutorial errata file alongside the tutorial.
  * Email change: Rene Mayorga -> rmayorga@debian.org
  * Update debian/copyright formatting.

 -- Alessandro Ghedini <al3xbio@gmail.com>  Wed, 04 May 2011 20:38:06 +0200

libevent-perl (1.12-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Rene Mayorga ]
  * debian/control: update my email address.

  [ Nathan Handler ]
  * New upstream release
  * debian/watch:
    - Update to ignore development releases.
  * debian/compat:
    - Bump to 7
  * debian/control:
    - Bump Standards-Version to 3.8.3
    - Add myself to list of Uploaders
    - Bump debhelper Build-Depends to >= 7
  * debian/rules:
    - Switch to short rules format
  * debian/docs:
    - Add file to install Tutorial.pdf

  [ gregor herrmann ]
  * debian/control:
    - remove version from perl build dependency as allowed since Policy 3.8.3
    - make short description a noun phrase
    - slightly rephrase long description in order to point out the module name
  * Update debian/copyright.

 -- gregor herrmann <gregoa@debian.org>  Thu, 27 Aug 2009 17:31:28 +0200

libevent-perl (1.11-1) unstable; urgency=low

  * New upstream release.
  * Refresh debian/rules, no functional changes; install demo/* as examples.
  * debian/copyright: rearrange "Files: " stanzas and extend information
    about the packaging.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Mon, 19 May 2008 20:56:35 +0200

libevent-perl (1.10-1) unstable; urgency=low

  [ Martín Ferrari ]
  * Starting taking over by DPG. See
  http://lists.debian.org/debian-perl/2008/02/msg00053.html.
  * Added watchfile.

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Changed:
    Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org> (was: Steve Kowalik
    <stevenk@debian.org>).

  [ Rene Mayorga ]
  * New Upstream version
  * debian/control
    + Bump standards-version to 3.7.3 ( no changes needed )
    + Raise compat level to 6
    + Add Uploaders field and myself
    + Add ${misc:Depends} and {shlibs:Depends} to Depends field
  * debian/rules - sync it with the last dh-make-perl template
  * Don't install README
  * remove debian/docs
  * rewrite debian/copyright
  * remove the patch introduced by the previos maintainer
    - this patch was added to avoid some warnings

 -- Rene Mayorga <rmayorga@debian.org.sv>  Thu, 21 Feb 2008 20:21:10 -0600

libevent-perl (1.06-1) unstable; urgency=low

  * New upstream release.
    + Fixes a memory leak. (Closes: #324662)

 -- Steve Kowalik <stevenk@debian.org>  Tue, 23 Aug 2005 22:12:13 +1000

libevent-perl (1.04-1) unstable; urgency=medium

  * New upstream release. (Closes: #303594)

 -- Steve Kowalik <stevenk@debian.org>  Sat,  9 Apr 2005 19:47:24 +1000

libevent-perl (1.02-1) unstable; urgency=low

  * New upstream release.

 -- Steve Kowalik <stevenk@debian.org>  Sun,  2 Jan 2005 02:49:08 +1100

libevent-perl (1.00-2) unstable; urgency=low

  * Change Section to perl.
  * Correct spelling error in long description. (Closes: #263788)
  * Fix up the rules file a bit.

 -- Steve Kowalik <stevenk@debian.org>  Fri,  6 Aug 2004 09:49:26 +1000

libevent-perl (1.00-1) unstable; urgency=low

  * New upstream release.
  * Move to debhelper compat level 4, change build-deps accordingly.
  * Bump Standards-Version to 3.6.1; no changes needed.

 -- Steve Kowalik <stevenk@debian.org>  Mon,  2 Aug 2004 10:01:48 +1000

libevent-perl (0.87-1) unstable; urgency=low

  * New upstream release.
  * Recompile to pick up perlapi-5.8 depends. (Closes: #186497)
  * Drop Build-Depends on perl to 5.6.1-16. (Now go away, Gus.)
  * Drop dh_undocumented from debian/rules.
  * Kick to Standards-Version 3.5.9; no changes needed.

 -- Steve Kowalik <stevenk@debian.org>  Fri, 28 Mar 2003 17:14:25 +1100

libevent-perl (0.86+1-2) unstable; urgency=low

  * Clean up some of the warnings as best I can.
  * Stuff 'make test' into its own target in debian/rules.
  * Recommend libtime-hires-perl.

 -- Steve Kowalik <stevenk@debian.org>  Mon, 21 Oct 2002 22:17:16 +1000

libevent-perl (0.86+1-1) unstable; urgency=low

  * Whoops, this package is not binary-indep. Change to Build-Depends, from
    Build-Depends-Indep. (Closes: #165426)
  * Sigh, this package was marked as native, fixing.

 -- Steve Kowalik <stevenk@debian.org>  Sat, 19 Oct 2002 11:51:49 +1000

libevent-perl (0.86-1) unstable; urgency=low

  * Initial release. (Closes: #158013)
  * Many, many thanks to Brendan O'Dea and Colin Watson for their help in
    fixing the fails-to-build-with-5.8 bug.

 -- Steve Kowalik <stevenk@debian.org>  Wed, 16 Oct 2002 22:32:05 +1000
